<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $content)
    {
       $this->name=$name;
       $this->content=$content;
       $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
        $name = $this->name;
        $content =$this->content;
        $email= $this->email;
        // dd($email);
        return $this->view('test.mailTest', compact('name', 'content', 'email'));
    }
}

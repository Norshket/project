<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    use HasFactory;
    // //если название таблицы не соответствует кправилам 
    // protected $table ='me_post';
    // //если поле первичного ключа называется иначе то 
    // protected $primaryKey = 'post_id';
    // //если поле не автоинкрементируемое
    // public $incrementing = false;
    // //устанавливает тип поля
    // protected $keyType = 'srting';

    protected $fillable =['title', 'content', 'rubric_id']; 
    public function getPostDate(){
        return Carbon:: parse($this->created_at)->diffForHumans();
    }

    public function rubric()
    {
        return $this->belongTo(Rubric::class);
    }
}

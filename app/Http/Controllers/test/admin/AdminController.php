<?php

namespace App\Http\Controllers\test\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return 'admin';
    }
}

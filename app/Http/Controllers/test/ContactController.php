<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact()
    {
        if(!empty($_POST)){        
            dump($_POST);    
            return 'Форма контактов отправлена';
        }
    
        return view('test.contact');
    }

    public function send(Request $request)
    {
        if($request->method() == 'POST'){
        $name=$request->input('name');
        $email=$request->input('email');
        $content = $request->input('content');

        Mail::to("$email")->send(new TestMail($name, $email, $content) );
        $request->session()->flash('success', 'Сообщение отправлено');
        return redirect('/send');
        } 

        return view('test.send');
    }
}

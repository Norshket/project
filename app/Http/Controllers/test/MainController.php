<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Rubric;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function index(Request $request)
    {     
       
        //создание куки
        //   Cookie::queue('test', 'Test cookie', 2);
        //   вывод куки
        //   dump(Cookie::get('test'));
        //   dump($request->cookie('test'));
        //   Удаление куки
        //   Cookie::queue(Cookie::forget('test'));
        

        //создание элементав сессии
        // $request->session()->put('test', 'Test Value');
        // session(
        //     ['cart'=>[
        //         ['id'=>1, 'title'=>'product 1'],
        //         ['id'=>2, 'title'=>'product 2']
        // ]]);
        
        //вывел элемент сверху
        // dump(session('test'));
        // dump(session('cart')[1]['title']);
        // dump(session()->all()); 
        
        //Добавил элемент в сессию
        // $request->session()->push('cart',['id'=>3, 'title'=>'Product 3']);


        //Для добавления и моментального удаления из сессии 
        // dump($request->session()->pull('test'));
        
        //для удаления элемента из сессии 
        // $request->session()->forget('test');

        // $request->session()->flush();      
        
        /*запись в кеш,  ключ значение время в секундах     
        если не ставить время то кешь будет вечным   
        */
        // Cache::put('key','value', 360);

        
        //добавление в кэш 
        // if(Cache::has('posts')){
        //     $posts=Cache::get('posts');
        // }else{
        $posts=Post::orderBy('id','desc')->paginate(3);
        // Cache::put('posts',$posts);
        // }


        //Удаление 
        //вариант 1
        // dump(Cache::get('key'));
        // dump(Cache::pull('key'));

        //вариант 2 
        // Cache::forget('key');
        // dump(Cache::get('key'));
        
        // вариант 3 удаление всего из кэша
        // Cache:: flush();



        // $posts=Post::orderBy('id','desc')->get();

        $title='Главная страница';
        return view('test.index', compact('title', 'posts'));
    }

    public function create()
    {
        $title='Создание поста';

        $rubrics= Rubric::pluck('title' , 'id')->all();
        return view('test.create',compact('title', 'rubrics'));
    }
     
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|min:5|max:100',
            'content'=>'required',
            'rubric_id'=>'integer',
        ]);
        

        //валидация 
        //что бы не менять её здесь топай в resources/lang/ создай папку скопируй туда содержимое en и правь его как хочешь
        // $rules=[
        //     'title'=>'required|min:5|max:100',
        //     'content'=>'required',
        //     'rubric_id'=>'integer'
        // ];
        
        // $messages=[
        //     'title.required'=>'Заполните поле заголовка', 
        //     'title.min'=>'Заголовок должен быть больше 5 символов',
        //     'title.max'=>'Взаголоке должно быть меньше 100 символов',
        //     'content.required'=> 'Заполните поле контента',
        //     'rubric_id.integer' => 'Выберите рубрику из списка'
        // ];

        // $validator = Validator::make($request->all(), $rules, $messages)->validate();

        Post::create($request->all());
        $request->session()->flash('success', 'Данные сохранены');
        return redirect()->route('main');
    }
     
}

<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about()
    {
        $title='About';
        return view('test.page.about', compact(['title']));
    }
}

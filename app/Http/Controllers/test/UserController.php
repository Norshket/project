<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PharIo\Manifest\Email;

class UserController extends Controller
{
    public function register()
    {
        return view( 'test.user.register');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=> 'required|confirmed',
            'img'=>'nullable|image',
        ]);

        //сохранение картинки какой файл-------куда---большее уточнение куда
        //либо указать в env FILESYSTEM_DRIVER = public
        if ($request->hasFile('img')){
            $folder = date('Y-m-d'); 
            $avatar = $request->file('img')->store('images');
        }

        $user = User::create([    
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'avatar'=> $avatar ?? null ,
        ]);
               
        Auth::login($user);  
        session()->flash('success', 'Регистрация успешна');
        return redirect()->route('main');
    }
    public function loginForm()
    {
        return view('test.user.login');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required',
        ]);

        if (Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password,
        ])){
           return redirect()->route('main');
        }else{
            return redirect()->back()->with('error','Некорректный логин или пароль');
        }
        
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.create');
    } 
}

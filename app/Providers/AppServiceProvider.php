<?php

namespace App\Providers;

use App\Models\Rubric;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // DB::listen(function($query){
        //     dump( $query->sql);
        // });      

        //------------------куда подключаем 
        view()->composer('test.layouts.footer',function ($view) {
            //---------что подключаем
            $view->with('rubrics',Rubric::all());
        });

    }
}

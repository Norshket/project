@extends('test.layouts.layouts')
@section('content')

@section('title')
@parent {{$title}}
@endsection

@section('header')
@parent
@endsection


<div class="pricing-header p-3 pb-md-4 mx-auto text-center">
    @isset($title)<h1 class="display-4 fw-normal">{{$title}}</h1>@endisset
    <p class="fs-5 text-muted">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.</p>
</div>
<div class="album py-5 bg-light">
    <div class="container">

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            @foreach($posts as $post )
            <div class="col-md-4">
                <div class="card shadow-sm">
                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">
                        <title>{{$post->title}}</title>
                        <rect width="100%" height="100%" fill="#55595c" /><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                    </svg>

                    <div class="card-body">
                        <p>{{$post->title}}</p>
                        <p class="card-text">{{$post->content}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                            </div>
                            <small class="text-muted">
                                <!-- {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('d-m-Y') }} -->
                                {{$post->getPostDate()}}
                            </small>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <nav aria-label="Page navigation example row">
            <ul class="pagination">
                {{ $posts->links() }}
            </ul>
        </nav>

    </div>
</div>

@endsection
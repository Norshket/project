<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">&copy; 2017–2021</small>
        </div>
        <div class="col-6 col-md">
            <h5>Features</h5>
            @if($rubrics)
            <ul class="list-unstyled text-small">
            @foreach($rubrics as $rubric )
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#">{{$rubric->title}}</a></li>
               @endforeach
            </ul>
            @endif
        </div>       
    
    </div>
</footer>
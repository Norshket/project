@extends('test.layouts.layouts')

@section('title')
@parent Регисрация
@stop

@section('content')
<form method="POST" action="{{route('register.store')}}" enctype="multipart/form-data">
  @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Введите имя</label>
    <input type="text" id="name" class="form-control"  name="name" value ="{{old ('name')}}">
  </div>

  <div class="mb-3">
    <label for="email" class="form-label">Введите Email</label>
    <input type="email" class="form-control"  name="email" id="email" value ="{{old ('email')}}"> 
  </div>

  <div class="mb-3">
    <label for="password" class="form-label">Введите пароль</label>
    <input class="form-control" type="password" id="password" name="password">
  </div>
  
  <div class="mb-3">
    <label for="password_confirmation" class="form-label">Введите пароль для подтверждения</label>
    <input class="form-control" type="password" id="password_confirmation" name="password_confirmation">
  </div>

  <div class="mb-3">
    <label for="img" class="form-label">Можете загрузить аватар</label>
    <input class="form-control-file" type="file" id="img" name="img">
  </div>

  <input type="submit" class="btn btn-primary" value="Отправить">

</form>
@stop
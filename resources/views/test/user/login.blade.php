@extends('test.layouts.layouts')

@section('title')
@parent Вход
@stop

@section('content')
<form method="POST" action="{{route('login.store')}}">
  @csrf
  <div class="mb-3">
    <label for="email" class="form-label">Введите Email</label>
    <input type="email" class="form-control"  name="email" id="email" value ="{{old ('email')}}"> 
  </div>

  <div class="mb-3">
    <label for="password" class="form-label">Введите пароль</label>
    <input class="form-control" type="password" id="password" name="password">
  </div>

  <input type="submit" class="btn btn-primary" value="Войти">

</form>
@stop
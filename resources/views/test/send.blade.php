@extends('test.layouts.layouts')
@section('content')
<form method="POST" action="{{route('send')}}">
  @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Введите имя</label>
    <input type="text" id="name" class="form-control"  name="name">
  </div>

  <div class="mb-3">
    <label for="email" class="form-label">Введите Email</label>
    <input type="email" class="form-control"  name="email" id="email">
  </div>

  <div class="mb-3">
    <label for="content" class="form-label">Введите сообщение</label>
    <textarea class="form-control" id="content" name="content" rows="3"></textarea>
  </div>
  <input type="submit" class="btn btn-primary" value="Отправить">

</form>
@stop

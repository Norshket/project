@extends('test.layouts.layouts')

@section('title')
@parent {{$title}}
@endsection


@section('content')


<form method="POST" action="{{route('posts.store')}}">
  @csrf
  <legend>{{$title}}</legend>
  <div class="mb-3">
    <label for="title" class="form-label">Тайтл</label>
    <input type="text" id="title" class="form-control" value="{{old('title')}}" name="title">
  </div>
  <div class="mb-3">
    <label for="content" class="form-label">Example textarea</label>
    <textarea class="form-control" id="content" name="content" rows="3">{{old('content')}}</textarea>
  </div>

  <div class="mb-3">
    <label for="rubric_id" class="form-label">Disabled select menu</label>
    <select id="rubric_id" name="rubric_id" class="form-select">
      <option>Выбрать рубрику</option>
      @foreach ($rubrics as $k=>$v)
      <option value="{{$k}}"
        @if(old ('rubric_id')== $k)selected @endif
        >{{$v}}</option>
      @endforeach
    </select>
  </div>
  <input type="submit" class="btn btn-primary" value="Отправить">

</form>
@endsection
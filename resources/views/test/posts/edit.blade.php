<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Изменить {{$id}}</p>
    <form action="{{route('posts.update', ['post'=>$id ] )}} " method="POST">
    @csrf
    @method('PUT')
    <input type="text" name='title'>
    <input type="submit" value="Отправить">
    
    </form>

</body>
</html>
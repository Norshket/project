<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<p>список постов</p>
<ul>
    <li>
    <a href="{{route('posts.show', ['post'=>1] )}}">ПОСТ 1</a>
    <a href="{{route('posts.create', ['post'=>1] )}}">Создания</a>
    <a href="{{route('posts.edit', ['post'=>1] )}}">Изменение</a>
    <form method="post" action="{{ route('posts.destroy',['post'=>1]) }}">
    @csrf
    @method('DELETE')
    <input type="submit" value="Удалить">

    </form>
    </li>
    
    <li>
    <a href="{{route('posts.show', ['post'=>2] )}}">ПОСТ 2</a>
    <a href="{{route('posts.create', ['post'=>2] )}}">Создания</a>
    <a href="{{route('posts.edit', ['post'=>2] )}}">Изменение</a>
    <form method="post" action="{{ route('posts.destroy' ,['post'=>2])}}">
    @csrf
    @method('DELETE')
    <input type="submit" value="Удалить">

    </form>
    </li>
    <li>
    <a href="{{route('posts.show', ['post'=>3] )}}">ПОСТ 3</a>
    <a href="{{route('posts.create', ['post'=>3] )}}">Создания</a>
    <a href="{{route('posts.edit', ['post'=>3] )}}">Изменение</a>
    <form method="post" action="{{route('posts.destroy' ,['post'=>3])}}">
    @csrf
    @method('DELETE')
    <input type="submit" value="Удалить">

    </form>
    </li>
</ul>
    
</body>
</html>
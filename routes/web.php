<?php

use App\Http\Controllers\test\ContactController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/contact', function () {
//     return view('test.contact');
// });
// Route::post('/addContact', function () {
//     dump($_POST);   
// })->name('addContact');

/* 
Или если нужно обработать запрос на одной странице 
то смотри ниже
*/
Route::match(['post','get'], '/contact','test\ContactController@contact')->name('contact');

 /*Группировка*/

//  

/*? - означает что эллемент не обязателен 
    важно задать значение по умолчанию 
*/
Route::get('/post/{id}/{slag?}', function($id ,$slag=null){
    return "Это пост с id $id словом $slag";
})
/*
либо так 
->where(
    [
        'id'=>'[0-9]+',
        'slag'=>'[a-zA-Z0-9]+'
    ]) 
    либо через уакзание правил   
    App\Providers\RouteServiceProvider\function boot
*/
;
// определяет есть ли  такая страница
Route::fallback(function(){
    //если нет то отпарвляет на главную
    return redirect()->route('main');
});

// Route::resource('/posts', 'test\PostController');



Route::group( ['middleware' => 'guest','namespace' => 'test'], function(){
    
Route::get('/register', 'UserController@register')->name('register');
Route::post('/register', 'UserController@store')->name('register.store');

Route::get('/login', 'UserController@loginForm')->name('login.create');
Route::post('/login', 'UserController@login')->name('login.store');


});

Route::group(['middleware' => 'auth', 'namespace' => 'test'], function(){
    Route::get('/logout', 'UserController@logout')->name('logout');
    Route::get('/', 'MainController@index')->name('main');
    Route::get('/create','MainController@create')->name('post.create');
    Route::post('/','MainController@store')->name('posts.store');
    Route::get('/about','PageController@about')->name('about');
    Route::match(['get','post'] , '/send','ContactController@send')->name('send');
});

Route::get('/admin','test\admin\AdminController@index')->middleware('admin')->name('admin');
